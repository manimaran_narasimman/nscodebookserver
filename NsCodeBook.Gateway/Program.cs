using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace NsCodeBook.Gateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)

                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseIISIntegration()
                              .UseStartup<Startup>()
                              .ConfigureLogging(configureLogging =>
                               {
                                   configureLogging.ClearProviders();
                                   configureLogging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                               })
                              //.UseUrls("http://192.168.28.58:6000")
                              .UseNLog();
                });
    }
}
