using System.Linq;
using NsCodeBook.Contract.Error;

using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace NsCodeBook.Gateway.Utils.Extensions
{
    public static class ModelStateExtensions
    {
        public static ErrorResponse GetModelErrorResponse(this ModelStateDictionary modelState)
        {
            return new ErrorResponse
            {
                Errors = modelState.Keys.SelectMany(key => modelState[key].Errors.Select(x => new Error(key, x.ErrorMessage)))
            };
        }
    }
}