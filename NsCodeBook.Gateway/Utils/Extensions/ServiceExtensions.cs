using System.Text;
using NsCodeBook.Business.Orchestrations;
using NsCodeBook.Business.Repositories;
using NsCodeBook.Contract.Classes;
using NsCodeBook.Contract.Data;
using NsCodeBook.Contract.Orchestrations;
using NsCodeBook.Contract.Repositories;
using NsCodeBook.Data.Implementations;
using NsCodeBook.Logging.Contracts;
using NsCodeBook.Logging.Implementations;
using NsCodeBook.Shared.Utils.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.IdentityModel.Tokens;

namespace NsCodeBook.Gateway.Utils.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {

            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<AccessToken>();
            services.AddSingleton<Hashing>();

            //Business 

            services.AddSingleton<IDemoUserRepository, DemoUserRepository>();
            services.AddSingleton<IDemoUserOrchestration, DemoUserOrchestration>();

            //Data

            services.AddSingleton<IDemoUserDataAccess, DemoUserDataAccess>();

            // services.AddSingleton<IAuthorizationHandler, SessionHandler>();

            return services;
        }

        public static IServiceCollection AuthorizationServices(
            this IServiceCollection services)
        {

            services.AddAuthorization(options =>
            {
                // sample policies
                options.AddPolicy("CreateDemoUserPolicy",
                    policy => policy.RequireClaim("ViewUser")
                    //.Requirements.Add(new SessonRequirement())
                );
            });

            return services;
        }

        public static IServiceCollection AuthenticationServices(
            this IServiceCollection services, IConfiguration configuration)
        {

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = "NsCodeBook.com",
                        ValidAudience = "NsCodeBook.com",
                        IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration.GetSection(Constants.SecurityConfig).GetSection(Constants.JwtKey).Value))

                    };

                });

            return services;
        }
    }
}