using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NsCodeBook.Contract.Classes;
using NsCodeBook.Contract.Error;
using NsCodeBook.Logging.Contracts;

namespace NsCodeBook.Gateway.Utils.Filters
{
    public class AppExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILoggerManager _logger;

        public AppExceptionMiddleware(RequestDelegate next, ILoggerManager logger)
        {
            this._next = next;
            this._logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var status = HttpStatusCode.InternalServerError;
            var message = Constants.Exception.InternalServerError;

            if (exception.GetType() == typeof(UnauthorizedAccessException))
            {
                message = exception.Message;
                status = HttpStatusCode.Unauthorized;
            }
            else if (exception.GetType() == typeof(NotImplementedException))
            {
                message = Constants.Exception.NotImplementedException;
                status = HttpStatusCode.NotImplemented;
            }
            else
            {
                message = Constants.Exception.InternalServerError;
                status = HttpStatusCode.InternalServerError;
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)status;

            string result = AppExceptionMiddleware.GetExceptionResponseBody(exception, context.Response.StatusCode, message);
            return context.Response.WriteAsync(result);
        }

        private static string GetExceptionResponseBody(Exception exception, int statusCode, String message)
        {
            var errorResponse = new ErrorResponse
            {
                Message = message
            };
            return JsonConvert.SerializeObject(errorResponse);
        }
    }
}