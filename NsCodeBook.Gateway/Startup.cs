using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using NsCodeBook.Contract.Classes;
using NsCodeBook.Gateway.Utils.Extensions;
using NsCodeBook.Gateway.Utils.Filters;
using NsCodeBook.Shared.AppSettings;

namespace NsCodeBook.Gateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(configure =>
            {
                configure.ReturnHttpNotAcceptable = true;
                // configure.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            })
            .AddNewtonsoftJson(setupAction =>
            {
                setupAction.UseCamelCasing(true);
            })
            // .AddJsonOptions(options =>
            // {
            //     options.JsonSerializerOptions.WriteIndented = true;
            // })
            .ConfigureApiBehaviorOptions(setupAction =>
            {
                //setupAction.SuppressModelStateInvalidFilter = true;
                setupAction.InvalidModelStateResponseFactory = (actionContext =>
                {
                    return new BadRequestObjectResult(actionContext.ModelState.GetModelErrorResponse());
                });
            });
            services.Configure<DbConfig>(Configuration.GetSection(Constants.AppSettings).GetSection(Constants.DbConfiguration));
            services.RegisterServices();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware<AppExceptionMiddleware>()
               .UseRouting()
               //.UseAuthorization()
               .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                })
                .UseCors(builder => builder.AllowAnyHeader()
                                           .AllowAnyMethod()
                                           .AllowAnyOrigin())
                .UseHttpsRedirection()
                .UseStaticFiles();

        }
    }
}
