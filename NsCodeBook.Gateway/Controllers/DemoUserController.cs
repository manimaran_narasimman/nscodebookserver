using System;
using NsCodeBook.Contract.Model.Request;
using NsCodeBook.Contract.Orchestrations;
using NsCodeBook.Logging.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using NsCodeBook.Contract.Error;

namespace NsCodeBook.Gateway.Controllers
{
    [Route("api")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private ILoggerManager _logger;
        private IDemoUserOrchestration _demoUserOrchestration;

        public DemoController(ILoggerManager logger, IDemoUserOrchestration sampleOrchestration) : base()
        {
            _demoUserOrchestration = sampleOrchestration;
            _logger = logger;
        }

        [HttpGet]
        [Route("ping")]
        public IActionResult Ping()
        {
            try
            {
                return Ok("Hello from demo controller");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("demo-users")]
        public IActionResult GetDemoUsers([FromQuery]DemoUserSearchRequest studentSearchRequest)
        {
            try
            {
                //INFO: Added global model state validation in the Startup.cs
                _logger.LogInfo(_logger.GetActivityLogContent("Category", "Module", "TaskPerformed", "Request", "Response", ""));
                var response = _demoUserOrchestration.GetDemoUsers(studentSearchRequest);
                _logger.LogInfo(_logger.GetActivityLogContent("Category", "Module", "TaskPerformed", "Request", "Response", ""));
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("demo-users/{demoUserKey}")]
        public IActionResult GetDemoUserByKey(long demoUserKey)
        {
            try
            {
                var response = _demoUserOrchestration.GetDemoUserByKey(demoUserKey);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("demo-users")]
        public IActionResult CreateDemoUser([FromBody]DemoUserRequest sampleRequest)
        {
            try
            {
                var response = _demoUserOrchestration.CreateDemoUser(sampleRequest);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("demo-users/{demoUserKey}")]
        public IActionResult UpdateDemoUser([FromBody]DemoUserRequest sampleRequest, [FromRoute]long demoUserKey)
        {
            try
            {
                if (sampleRequest.DemoUserKey == demoUserKey)
                {
                    var response = _demoUserOrchestration.UpdateDemoUser(sampleRequest, demoUserKey);
                    return Ok(response);
                }
                var errorResponse = new ErrorResponse
                {
                    Message = "Request parameter validation failed",
                };
                errorResponse.Errors.Append(new Error(nameof(demoUserKey), "DemoUser Key in the request body does not match with the DemoUserKey from the URL"));
                return BadRequest(errorResponse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        [Route("demo-users/{demoUserKey}")]
        public IActionResult DeleteDemoUser([FromRoute]long demoUserKey)
        {
            try
            {
                var response = _demoUserOrchestration.DeleteDemoUser(demoUserKey);
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}