using System;
using System.Collections.Generic;
using System.Data.Common;
using NsCodeBook.Contract.Classes;
using NsCodeBook.Contract.Data;
using NsCodeBook.Contract.Model.Request;
using NsCodeBook.Contract.Model.Response;
using NsCodeBook.Contract.Repositories;
using NsCodeBook.Logging.Contracts;

namespace NsCodeBook.Business.Repositories
{
    public class DemoUserRepository : IDemoUserRepository
    {
        private IDemoUserDataAccess _demoUserDataAccess;
        private ILoggerManager _logger;
        public DemoUserRepository(IDemoUserDataAccess demoUserDataAccess, ILoggerManager logger)
        {
            _demoUserDataAccess = demoUserDataAccess;
            _logger = logger;
        }

        public List<DemoUserResponse> GetDemoUsers()
        {
            List<DemoUserResponse> result = new List<DemoUserResponse>();
            using (DbDataReader dataReader = _demoUserDataAccess.GetDemoUsers())
            {
                result = dataReader.ToCustomList<DemoUserResponse>();
                return result;
            }
        }

        public DemoUserSearchResponse GetDemoUsers(DemoUserSearchRequest demoUserSearchRequest)
        {
            DemoUserSearchResponse result = new DemoUserSearchResponse();
            using (DbDataReader dataReader = _demoUserDataAccess.GetDemoUsers(demoUserSearchRequest))
            {
                while (dataReader.Read())
                {
                    result.DemoUsers.Add(new DemoUserResponse
                    {
                        DemoUserKey = (long)dataReader["DemoUserKey"],
                        FirstName = (string)dataReader["FirstName"],
                        LastName = (string)dataReader["LastName"],
                        Standard = (string)dataReader["Standard"],
                    });
                    result.TotalRecord = (int)dataReader["TotalRecord"];
                }
                return result;
            }
        }

        public DemoUserResponse GetDemoUserByKey(long studentKey)
        {
            DemoUserResponse result = new DemoUserResponse();
            using (DbDataReader dataReader = _demoUserDataAccess.GetDemoUserByKey(studentKey))
            {
                result = dataReader.ToCustomEntity<DemoUserResponse>();
                return result;
            }
        }

        public DemoUserResponse CreateDemoUser(DemoUserRequest sampleRequest)
        {
            DemoUserResponse result = new DemoUserResponse();
            using (DbDataReader dataReader = _demoUserDataAccess.CreateDemoUser(sampleRequest))
            {
                result = dataReader.ToCustomEntity<DemoUserResponse>();
                return result;
            }
        }

        public DemoUserResponse UpdateDemoUser(DemoUserRequest sampleRequest, long studentKey)
        {
            DemoUserResponse result = new DemoUserResponse();
            using (DbDataReader dataReader = _demoUserDataAccess.UpdateDemoUser(sampleRequest, studentKey))
            {
                result = dataReader.ToCustomEntity<DemoUserResponse>();
                return result;
            }
        }

        public BaseResponse DeleteDemoUser(long studentKey)
        {
            BaseResponse result = new BaseResponse();
            using (DbDataReader dataReader = _demoUserDataAccess.DeleteDemoUser(studentKey))
            {
                if (dataReader.Read())
                {
                    result.IsSuccess = (bool)dataReader["IsDemoUserDeleted"];
                }
                return result;
            }
        }
    }
}