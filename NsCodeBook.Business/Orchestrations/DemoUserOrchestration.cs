using System.Collections.Generic;
using NsCodeBook.Contract.Model.Request;
using NsCodeBook.Contract.Model.Response;
using NsCodeBook.Contract.Orchestrations;
using NsCodeBook.Contract.Repositories;
using NsCodeBook.Logging.Contracts;

namespace NsCodeBook.Business.Orchestrations
{
    public class DemoUserOrchestration : IDemoUserOrchestration
    {
        private ILoggerManager _logger;
        private IDemoUserRepository _demoUserRepository;

        public DemoUserOrchestration(ILoggerManager logger, IDemoUserRepository demoUserRepository)
        {
            _demoUserRepository = demoUserRepository;
            _logger = logger;
        }

        public List<DemoUserResponse> GetDemoUsers()
        {
            return _demoUserRepository.GetDemoUsers();
        }

        public DemoUserSearchResponse GetDemoUsers(DemoUserSearchRequest studentSearchRequest)
        {
            return _demoUserRepository.GetDemoUsers(studentSearchRequest);
        }

        public DemoUserResponse GetDemoUserByKey(long studentKey)
        {
            return _demoUserRepository.GetDemoUserByKey(studentKey);
        }

        public DemoUserResponse CreateDemoUser(DemoUserRequest sampleRequest)
        {
            return _demoUserRepository.CreateDemoUser(sampleRequest);
        }

        public DemoUserResponse UpdateDemoUser(DemoUserRequest sampleRequest, long studentKey)
        {
            return _demoUserRepository.UpdateDemoUser(sampleRequest, studentKey);
        }

        public BaseResponse DeleteDemoUser(long studentKey)
        {
            return _demoUserRepository.DeleteDemoUser(studentKey);
        }
    }
}