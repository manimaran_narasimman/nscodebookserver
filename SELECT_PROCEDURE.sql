USE [DATABASE_NAME]
GO
/****** Object:  StoredProcedure [dbo].[GetStudentByKey]    Script Date: 12-11-2019 17:10:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROCEDURE_NAME](
	-- STORED PROCEDURE PARAMETERS
	--@StudentKey BIGINT
)
AS
/***************************************************************************
$Header:$ 
** Name: [PROCEDURE_NAME]
** Object Type: Stored Procedure
** Application: Application name 
** Description: Used to get a student record by Studentkey
** Dependencies: No Dependencies.
** Parameters: @StudentKey --> Identity key of a student record
** Status Returns: Returns a student record based on the value of @StudentKey
** Example Call:   exec [dbo].[GetStudentByKey] 2
** Modification History:
** -----------------------------------------------------------------------
**
** **********************************************************************/
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL  READ COMMITTED 

DECLARE @ErrorNumber INT,
		@ErrorMessage VARCHAR(max),
		@ErrorState INT,
		@ErrorSeverity INT;

BEGIN TRY
	
	-- SELECT Statements...

END TRY
BEGIN CATCH

	SET @ErrorNumber = ERROR_NUMBER(); 
	SET @ErrorMessage  = ERROR_MESSAGE(); 
	SET @ErrorState =ERROR_STATE(); 
	SET @ErrorSeverity =ERROR_SEVERITY();
	RAISERROR ('%d | %s',@ErrorSeverity,@ErrorState,@ErrorNumber, @ErrorMessage);

END CATCH
END
