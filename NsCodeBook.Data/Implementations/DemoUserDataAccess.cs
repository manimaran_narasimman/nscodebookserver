using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using NsCodeBook.Contract.Data;
using NsCodeBook.Contract.Model.Request;
using NsCodeBook.DataAccess.SqlDb.Implementations;
using NsCodeBook.Shared.AppSettings;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using System;
using NsCodeBook.Contract.Classes;

namespace NsCodeBook.Data.Implementations
{
    public class DemoUserDataAccess : SqlDataAccess, IDemoUserDataAccess
    {
        public DemoUserDataAccess(IOptions<DbConfig> dbConfig) : base(dbConfig) { }

        public DbDataReader GetDemoUsers()
        {
            string procedureName = "[dbo].[GetDemoUsers]";
            return GetDataReader(procedureName, null, CommandType.StoredProcedure);
        }

        public DbDataReader GetDemoUsers(DemoUserSearchRequest studentSearchRequest)
        {
            string procedureName = "[dbo].[GetDemoUserDetail]";
            List<DbParameter> dbParameters = new List<DbParameter>();
            dbParameters.Add(new SqlParameter { ParameterName = "@SearchKey", Value = studentSearchRequest.SearchKey, SqlDbType = SqlDbType.Int });
            dbParameters.Add(new SqlParameter { ParameterName = "@SearchValue", Value = studentSearchRequest.SearchValue, SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@SortColumn", Value = studentSearchRequest.SortColumn, SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@SortOrder", Value = studentSearchRequest.SortOrder.GetDescription(), SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@PageNumber", Value = studentSearchRequest.PageNumber, SqlDbType = SqlDbType.Int });
            dbParameters.Add(new SqlParameter { ParameterName = "@PageLength", Value = studentSearchRequest.PageLength, SqlDbType = SqlDbType.Int });
            return GetDataReader(procedureName, dbParameters, CommandType.StoredProcedure);
        }

        public DbDataReader GetDemoUserByKey(long studentKey)
        {
            string procedureName = "[dbo].[GetDemoUserByKey]";
            List<DbParameter> dbParameters = new List<DbParameter>();
            dbParameters.Add(new SqlParameter { ParameterName = "@DemoUserKey", Value = studentKey, SqlDbType = SqlDbType.BigInt });
            return GetDataReader(procedureName, dbParameters, CommandType.StoredProcedure);
        }

        public DbDataReader CreateDemoUser(DemoUserRequest sampleRequest)
        {
            string procedureName = "[dbo].[CreateDemoUser]";
            List<DbParameter> dbParameters = new List<DbParameter>();
            dbParameters.Add(new SqlParameter { ParameterName = "@FirstName", Value = sampleRequest.FirstName, SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@LastName", Value = sampleRequest.LastName, SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@Standard", Value = sampleRequest.Standard, SqlDbType = SqlDbType.VarChar });
            return GetDataReader(procedureName, dbParameters, CommandType.StoredProcedure);
        }

        public DbDataReader UpdateDemoUser(DemoUserRequest sampleRequest, long studentKey)
        {
            string procedureName = "[dbo].[UpdateDemoUser]";
            List<DbParameter> dbParameters = new List<DbParameter>();
            dbParameters.Add(new SqlParameter { ParameterName = "@DemoUserKey", Value = studentKey, SqlDbType = SqlDbType.BigInt });
            dbParameters.Add(new SqlParameter { ParameterName = "@FirstName", Value = sampleRequest.FirstName, SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@LastName", Value = sampleRequest.LastName, SqlDbType = SqlDbType.VarChar });
            dbParameters.Add(new SqlParameter { ParameterName = "@Standard", Value = sampleRequest.Standard, SqlDbType = SqlDbType.VarChar });
            return GetDataReader(procedureName, dbParameters, CommandType.StoredProcedure);
        }

        public DbDataReader DeleteDemoUser(long studentKey)
        {
            string procedureName = "[dbo].[DeleteDemoUser]";
            List<DbParameter> dbParameters = new List<DbParameter>();
            dbParameters.Add(new SqlParameter { ParameterName = "@DemoUserKey", Value = studentKey, SqlDbType = SqlDbType.BigInt });
            return GetDataReader(procedureName, dbParameters, CommandType.StoredProcedure);
        }
    }
}