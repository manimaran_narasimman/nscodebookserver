using System;
using System.Reflection;
using NsCodeBook.Logging.Contracts;
using NLog;

namespace NsCodeBook.Logging.Implementations
{
    public class LoggerManager : ILoggerManager
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        public LoggerManager() { }

        public void LogDebug(string message)
        {
            logger.Debug(message);
        }

        public void LogError(string message)
        {
            logger.Error(message);
        }

        public void LogInfo(string message)
        {
            logger.Info(message);
        }

        public void LogWarn(string message)
        {
            logger.Warn(message);
        }

        public string GetActivityLogContent(string category, string module, string request, string responseStatus, string response, string userEmail)
        {
            string result = string.Format("[Category:{0}][Module:{1}],[Request:{2}],[ResponseStatus:{3}],[Response:{4}],[LoggedInUserEmail:{5}]", category, module, request, responseStatus, response, userEmail);
            result = result.Replace(System.Environment.NewLine, "    ");
            return result;
        }

        public string GetExceptionLogContent(Exception exception)
        {
            MethodBase site = exception.TargetSite;
            var MethodName = site.Name;
            var SourcePath = site.DeclaringType.FullName;
            var Message = exception.Message;
            var Stacktrace = exception.StackTrace.ToString();
            string result = string.Format("[Message:{0}][SourcePath:{1}],[MethodName:{2}][Stacktrace:{3}]", Message, SourcePath, MethodName, Stacktrace);
            result = result.Replace(System.Environment.NewLine, "    ");
            return result;
        }

    }
}