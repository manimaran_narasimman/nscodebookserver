using System;

namespace NsCodeBook.Logging.Contracts
{
    public interface ILoggerManager
    {
        void LogInfo(string message);
        void LogWarn(string message);
        void LogDebug(string message);
        void LogError(string message);
        string GetActivityLogContent(string category, string module, string taskPerformed, string request, string response, string userEmail);
        string GetExceptionLogContent(Exception exception);
    }
}