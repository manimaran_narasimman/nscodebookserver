using System.ComponentModel;
namespace NsCodeBook.Contract.Classes
{
    public enum SortOrder
    {
        [Description("ASC")]
        Ascending = 1,
        [Description("DESC")]
        Descending = 2
    }
}