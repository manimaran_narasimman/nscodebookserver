using System.Collections;
using System.Collections.Generic;
using NsCodeBook.Contract.Model.Data;

namespace NsCodeBook.Contract.Classes
{
    public static class Constants
    {
        public const string AppSettings = "AppSettings";
        public const string DbConfiguration = "DbConfiguration";
        public const string SecurityConfig = "SecurityConfig";
        public const string JwtKey = "JwtKey";
        public const string Salt = "Salt";

        public static class Exception
        {
            public const string InternalServerError = "Internal Server Error";
            public const string UnauthorizedAccess = "Unauthorized Access";
            public const string NotImplementedException = "Not Implemented";
        }
    }
}