using System;
using System.ComponentModel.DataAnnotations;
using NsCodeBook.Contract.Classes;
using NsCodeBook.Contract.Validations;

namespace NsCodeBook.Contract.Validations
{
    public class GradeValidationAttribute : ValidationAttribute
    {
        public GradeValidationAttribute()
        {
        }

        public string ErrorMsg { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // add your validation logic, below is just a sample
            if (string.IsNullOrWhiteSpace(value?.ToString()))
            {
                return new ValidationResult(ErrorMsg);
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}