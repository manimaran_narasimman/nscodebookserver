using System.Data.Common;
using NsCodeBook.Contract.Model.Request;

namespace NsCodeBook.Contract.Data
{
    public interface IDemoUserDataAccess
    {
        DbDataReader GetDemoUsers();
        DbDataReader GetDemoUsers(DemoUserSearchRequest demoUserSearchRequest);
        DbDataReader GetDemoUserByKey(long demoUserKey);
        DbDataReader CreateDemoUser(DemoUserRequest demoUserRequest);
        DbDataReader UpdateDemoUser(DemoUserRequest sampleRequest, long demoUserKey);
        DbDataReader DeleteDemoUser(long demoUserKey);
    }
}