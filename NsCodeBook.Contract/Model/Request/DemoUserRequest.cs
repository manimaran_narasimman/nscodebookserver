using System;
using System.ComponentModel.DataAnnotations;
using NsCodeBook.Contract.Classes;
using NsCodeBook.Contract.Model.Data;
using NsCodeBook.Contract.Validations;

namespace NsCodeBook.Contract.Model.Request
{
    public class DemoUserRequest
    {
        public long DemoUserKey { get; set; }

        [Required(ErrorMessage = "FirstName is Required")]
        [MaxLength(ErrorMessage = "Length should be lesser than or equal to 25 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is Required")]
        [MaxLength(ErrorMessage = "Length should be lesser than or equal to 25 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Standard is Required")]
        [MaxLength(ErrorMessage = "Length should be lesser than or equal to 25 characters")]
        [GradeValidation]
        public string Standard { get; set; }
    }

    public class DemoUserSearchRequest
    {
        public int SearchKey { get; set; } = 0;
        public string SearchValue { get; set; } = string.Empty;
        public string SortColumn { get; set; } = "FirstName";
        public SortOrder SortOrder { get; set; } = SortOrder.Ascending;
        [Required]
        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }
        [Range(10, 1000)]
        public int PageLength { get; set; } = 10;
    }
}