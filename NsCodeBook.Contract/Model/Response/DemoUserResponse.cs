
using System.Collections.Generic;

namespace NsCodeBook.Contract.Model.Response
{
    public class DemoUserResponse : BaseResponse
    {
        // Properties which we need to send as a response to the requested client
        public long DemoUserKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Standard { get; set; }
    }

    public class DemoUserSearchResponse
    {
        public List<DemoUserResponse> DemoUsers { get; set; } = new List<DemoUserResponse>();
        public int TotalRecord { get; set; }
    }
}