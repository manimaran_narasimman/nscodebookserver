using System.Collections.Generic;
using System.Text.Json;

namespace NsCodeBook.Contract.Error
{
    public class ErrorResponse
    {
        public string Message { get; set; } = "Model validation failed";
        public IEnumerable<Error> Errors { get; set; } = new List<Error>();
    }
}