namespace NsCodeBook.Contract.Error
{
    public class Error
    {
        public string Key { get; set; }
        public string Message { get; set; }

        public Error(string key, string message)
        {
            this.Key = key;
            this.Message = message;
        }
    }
}