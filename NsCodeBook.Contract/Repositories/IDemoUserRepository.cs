using System.Collections.Generic;
using NsCodeBook.Contract.Model.Request;
using NsCodeBook.Contract.Model.Response;

namespace NsCodeBook.Contract.Repositories
{
    public interface IDemoUserRepository
    {
        List<DemoUserResponse> GetDemoUsers();
        DemoUserSearchResponse GetDemoUsers(DemoUserSearchRequest studentSearchRequest);
        DemoUserResponse GetDemoUserByKey(long studentKey);
        DemoUserResponse CreateDemoUser(DemoUserRequest sampleRequest);
        DemoUserResponse UpdateDemoUser(DemoUserRequest sampleRequest, long studentKey);
        BaseResponse DeleteDemoUser(long studentKey);
    }
}