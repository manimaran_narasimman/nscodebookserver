using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;

namespace NsCodeBook.DataAccess.Contracts
{
    public interface ISqlDataAccess
    {
        int ExecuteNonQuery(string commandText, List<DbParameter> parameters, CommandType commandType);
        object ExecuteScalar(string commandText, List<DbParameter> parameters);
        DbDataReader GetDataReader(string commandText, List<DbParameter> parameters, CommandType commandType);
        DataSet GetDataSet(string commandText, List<DbParameter> parameters, CommandType commandType);
        DataTable GetDataTable(string commandText, List<DbParameter> parameters, CommandType commandType);
    }
}
