using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using NsCodeBook.DataAccess.Contracts;
using NsCodeBook.Shared.AppSettings;
using NsCodeBook.Shared.Utils;
using Microsoft.Extensions.Options;

namespace NsCodeBook.DataAccess.SqlDb.Implementations
{
    public abstract class SqlDataAccess : ISqlDataAccess
    {
        private DbConfig _dbConfiguration;
        public SqlDataAccess(IOptions<DbConfig> dbConfig)
        {
            _dbConfiguration = dbConfig.Value;
        }

        public int ExecuteNonQuery(string commandText, List<DbParameter> parameters, CommandType commandType = CommandType.Text)
        {
            try
            {
                using (SqlConnection connection = this.GetConnection())
                {
                    DbCommand cmd = this.GetCommand(connection, commandText, commandType);
                    UpdateDbNullValues(parameters);
                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ExecuteScalar(string commandText, List<DbParameter> parameters)
        {
            try
            {
                using (SqlConnection connection = this.GetConnection())
                {
                    DbCommand cmd = this.GetCommand(connection, commandText, CommandType.Text);
                    UpdateDbNullValues(parameters);
                    if (parameters != null && parameters.Count > 0)
                    {
                        cmd.Parameters.AddRange(parameters.ToArray());
                    }
                    return cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DbDataReader GetDataReader(string commandText, List<DbParameter> parameters, CommandType commandType = CommandType.Text)
        {
            try
            {
                SqlConnection connection = this.GetConnection();
                DbCommand cmd = this.GetCommand(connection, commandText, commandType);
                UpdateDbNullValues(parameters);
                if (parameters != null && parameters.Count > 0)
                {
                    cmd.Parameters.AddRange(parameters.ToArray());
                }
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetDataSet(string commandText, List<DbParameter> parameters = null, CommandType commandType = CommandType.Text)
        {
            try
            {
                using (SqlConnection connection = this.GetConnection())
                {
                    using (var cmd = new SqlCommand(commandText, connection))
                    {
                        UpdateDbNullValues(parameters);
                        if (parameters != null && parameters.Count > 0)
                        {
                            cmd.Parameters.AddRange(parameters.ToArray());
                        }
                        using (var adapter = new SqlDataAdapter(cmd))
                        {

                            var dataset = new DataSet();
                            var resultTable = new DataTable();
                            adapter.Fill(dataset);

                            return dataset;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetDataTable(string commandText, List<DbParameter> parameters = null, CommandType commandType = CommandType.Text)
        {
            try
            {
                using (SqlConnection connection = this.GetConnection())
                {

                    using (var cmd = new SqlCommand(commandText, connection))
                    {
                        UpdateDbNullValues(parameters);
                        using (var adapter = new SqlDataAdapter(cmd))
                        {

                            if (parameters != null && parameters.Count > 0)
                            {
                                adapter.SelectCommand.Parameters.AddRange(parameters.ToArray());
                            }

                            var dataTable = new DataTable();
                            adapter.Fill(dataTable);
                            return dataTable;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region Private Methods
        private SqlConnection GetConnection()
        {
            SqlConnection connection = new SqlConnection(_dbConfiguration.DefaultConnectionString);
            if (connection.State != ConnectionState.Open)
                connection.Open();
            return connection;
        }

        private void UpdateDbNullValues(IList<DbParameter> parameterValues)
        {
            if (parameterValues != null)
            {
                // Iterate through all the parameters and replace null values with DbNull.Value
                foreach (DbParameter sqlParameter in parameterValues)
                {
                    if ((sqlParameter.Value is string && string.IsNullOrEmpty(sqlParameter.Value.ToString())) || sqlParameter.Value == null)
                    {
                        sqlParameter.Value = DBNull.Value;
                    }
                }
            }
        }

        private DbCommand GetCommand(DbConnection connection, string commandText, CommandType commandType)
        {
            SqlCommand command = new SqlCommand(commandText, connection as SqlConnection);
            command.CommandType = commandType;
            command.CommandTimeout = 1000;
            return command;
        }
        #endregion
    }
}