USE [DATABASE_NAME]
GO
/****** Object:  StoredProcedure [dbo].[CreateStudent]    Script Date: 12-11-2019 17:07:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROCEDURE_NAME](
    -- STORED PROCEDURE PARAMETERS
	--@FirstName VARCHAR(25),
	--@LastName VARCHAR(25),
	--@Standard VARCHAR(25)
)
AS
/***************************************************************************
$Header:$ 
** Name: [CreateStudent]
** Object Type: Stored Procedure
** Application: Internal - Student Register 
** Description: Used to create a student record
** Dependencies: No Dependencies.
** Parameters: @FirstName --> First name of the student,
			   @LastName --> Last name of the student,
			   @Standard --> Standard of the studnet
				
** Status Returns: The newly created student record
** Example Call: exec [dbo].[CreateStudent] 'FName','LName','10th Grade'
** Modification History:
** -----------------------------------------------------------------------
**  
** **********************************************************************/
BEGIN
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL  READ COMMITTED 

DECLARE @ErrorNumber INT,
		@ErrorMessage VARCHAR(max),
		@ErrorState INT,
		@ErrorSeverity INT;

BEGIN TRANSACTION
BEGIN TRY

	-- INSERT/UPDATE/DELETE Statements

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;
	SET @ErrorNumber = ERROR_NUMBER(); 
	SET @ErrorMessage  = ERROR_MESSAGE(); 
	SET @ErrorState =ERROR_STATE(); 
	SET @ErrorSeverity =ERROR_SEVERITY();
	RAISERROR ('%d | %s',@ErrorSeverity,@ErrorState,@ErrorNumber, @ErrorMessage);

END CATCH
END